```html
<label class="form-label form-label-top form-label-auto" id="label_33" for="input_33">
Agama
<span class="form-required">
*
</span>
</label>
<div id="cid_33" class="form-input-wide jf-required">
<select class="form-dropdown validate[required] form-validation-error" id="input_33" name="q33_agama" style="width:150px" data-component="dropdown" required="" aria-labelledby="label_33">
<option value="">  </option>
<option value="Islam"> Islam </option>
<option value="Kristen"> Kristen </option>
<option value="Hindu"> Hindu </option>
<option value="Budha"> Budha </option>
<option value="Lainnya"> Lainnya </option>
</select>
<div class="form-error-message"> This field is required.<div class="form-error-arrow"><div class="form-error-arrow-inner"></div></div></div>
```